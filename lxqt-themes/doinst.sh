if [ -r usr/share/icons/hicolor/icon-theme.cache ]; then
  if [ -x usr/bin/gtk-update-icon-cache ]; then
    usr/bin/gtk-update-icon-cache -q usr/share/icons/hicolor >/dev/null 2>&1
  fi
fi
