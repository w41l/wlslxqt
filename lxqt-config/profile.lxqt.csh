#!/bin/csh
# LXQt additions:
if ( ! $?LXQTDIRS ) then
    setenv LXQTDIRS /usr
endif
setenv PATH ${PATH}:/usr/lib64/kf5

if ( $?XDG_CONFIG_DIRS ) then
    setenv XDG_CONFIG_DIRS ${XDG_CONFIG_DIRS}:/etc/lxqt/xdg
else
    setenv XDG_CONFIG_DIRS /etc/xdg:/etc/lxqt/xdg
endif

if ( ! $?XDG_RUNTIME_DIR ) then
    # Using /run/user would be more in line with XDG specs, but in that case
    # we should mount /run as tmpfs and add this to the Slackware rc scripts:
    # mkdir /run/user ; chmod 1777 /run/user
    # setenv XDG_RUNTIME_DIR /run/user/$USER
    setenv XDG_RUNTIME_DIR /tmp/xdg-runtime-$USER
    mkdir -p $XDG_RUNTIME_DIR
    chown $USER $XDG_RUNTIME_DIR
    chmod 700 $XDG_RUNTIME_DIR
endif
