WLSBuild for LXQt Desktop
=========================

Step to build your own LXQt Desktop using my SlackBuild:

1. For KDE Framework 5 please see file [kdeplasma.lst](kdeplasma.lst).
2. Then you can run ./wlslxqt.SlackBuild to start building and install LXQt Desktop.
4. After finish, run xwmconfig and select lxqt.
5. Enjoy LXQt.

LICENSE
=======
Please read [LICENSE](LICENSE).
